mod blip_buf_units;
mod units;

use crate::units::*;
use blip_buf_units::BlipBufUnits;
use divrem::DivCeil;
use sdl2::audio::{AudioQueue, AudioSpec, AudioSpecDesired};
use std::sync::{Arc, Mutex};
use std::time::Duration;

type Amplitude = i16;

// https://wiki.nesdev.com/w/index.php/CPU
// >Emulator authors may wish to emulate the NTSC NES/Famicom CPU at 21441960 Hz...
// >to ensure a synchronised/stable 60 frames per second.[2]
pub const MASTER_CLK_PER_S: ClkPerS<f64> = 21441960.;

// fn from_float(value: f32) -> Amplitude {
//     (value * Amplitude::max_value() as f32) as Amplitude
// }

// const WAVESMP_PER_CYC: u8 = 8;

// fn period_to_cpuclk_per_wavesmp(period: u16) -> u32 {
//     // period is 11-bit.
//     // multiplying by <<4 is 15-bit.
//     // Return u32 to avoid worrying about overflow.
//     16 * (period as u32 + 1)
// }

// const WAVE_TABLE_2A03: [[u8; 8]; 4] = [
//     [0, 0, 0, 0, 0, 0, 0, 1],
//     [0, 0, 0, 0, 0, 0, 1, 1],
//     [0, 0, 0, 0, 1, 1, 1, 1],
//     [1, 1, 1, 1, 1, 1, 0, 0],
// ];

// impl SquareWave {
//     fn new(blip: MutexBlipBuf, _frequency: CycPerS<f32>, volume: f32) -> SquareWave {
//         SquareWave {
//             blip: blip,
//             period: 0x1AB,
//             wavesmp_partial: 0,
//             wavesmp_pos: 0,
//             wavesmp_per_cyc: 32,
//             volume: from_float(volume),
//         }
//     }
// }

// struct SquareWave {
//     blip: MutexBlipBuf,
//     period: u16,
//     wavesmp_partial: u16, // always within 0..period
//     wavesmp_pos: u16,
//     wavesmp_per_cyc: u16,
//     volume: Amplitude,
// }

// impl SquareWave {
// }

const MIN_FRAME_PER_S: u32 = 16;

fn max_smp_per_frame(smp_s: u32) -> u32 {
    smp_s.div_ceil(MIN_FRAME_PER_S)
}

type SharedBlipBuf = Arc<Mutex<BlipBufUnits>>;

fn shared_blip_buf(spec: &AudioSpec) -> SharedBlipBuf {
    // spec.freq is i32 somehow.
    let smp_s = spec.freq as u32;

    let mut blip = BlipBufUnits::new(2 * max_smp_per_frame(smp_s));
    blip.set_rates(MASTER_CLK_PER_S, smp_s as f64);

    Arc::new(Mutex::new(blip))
}

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init().unwrap();
    let audio_subsystem = sdl_context.audio().unwrap();

    let desired_spec = AudioSpecDesired {
        freq: None,
        channels: Some(1), // mono
        samples: None,     // default sample size
    };

    // See https://github.com/Rust-SDL2/rust-sdl2/blob/master/examples/audio-queue-squarewave.rs
    let device: AudioQueue<Amplitude> = audio_subsystem.open_queue(None, &desired_spec)?;
    let spec: &AudioSpec = device.spec();

    let blip_shared: SharedBlipBuf = shared_blip_buf(spec);

    {
        let mut blip = blip_shared.lock().unwrap();
        blip.add_delta(0, 10000);
    }

    // Start playback
    device.resume();

    // Stop playback after main thread exits
    std::thread::sleep(Duration::from_millis(500));

    Ok(())
}
