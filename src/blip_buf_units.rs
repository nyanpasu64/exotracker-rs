use crate::units::*;
use blip_buf::BlipBuf;

pub use blip_buf::MAX_FRAME;
pub use blip_buf::MAX_RATIO;

pub type InAmplitude<T> = T;
pub type OutAmplitude<T> = T;

pub struct BlipBufUnits {
    blip_buf: BlipBuf,
}

impl BlipBufUnits {
    /// Creates new buffer that can hold at most `max_nsamp` samples. Sets rates
    /// so that there are `MAX_RATIO` clocks per sample. Returns pointer to new
    /// buffer, or panics if insufficient memory.
    pub fn new(max_nsamp: Smp<u32>) -> BlipBufUnits {
        // blip_buf.c:
        // enum { time_bits = pre_shift + 20 };
        // all "sample counts" are left-shifted by 20 bits. 4096+ samples will overflow internally.
        // blip_clocks_needed() will be wrong, maybe other issues too.
        // https://github.com/mvdnes/blip_buf-rs/issues/1
        assert!(max_nsamp < 4096);

        BlipBufUnits {
            blip_buf: BlipBuf::new(max_nsamp),
        }
    }

    /// Sets approximate input clock rate and output sample rate. For every
    /// `clk_per_s` input clocks, approximately `smp_per_s` samples are generated.
    pub fn set_rates(&mut self, clk_per_s: ClkPerS<f64>, smp_per_s: SmpPerS<f64>) {
        self.blip_buf.set_rates(clk_per_s, smp_per_s)
    }

    /// Clears entire buffer. Afterwards, `samples_avail() == 0`.
    pub fn clear(&mut self) {
        self.blip_buf.clear()
    }

    /// Adds positive/negative d_amplitude into buffer at specified clock time.
    pub fn add_delta(&mut self, clk_time: Clk<u32>, d_amplitude: InAmplitude<i32>) {
        self.blip_buf.add_delta(clk_time, d_amplitude)
    }

    /// Same as `add_delta()`, but uses faster, lower-quality synthesis.
    pub fn add_delta_fast(&mut self, clk_time: Clk<u32>, d_amplitude: InAmplitude<i32>) {
        self.blip_buf.add_delta_fast(clk_time, d_amplitude)
    }

    /// Length of time frame, in clocks, needed to make `out_nsamp` additional
    /// samples available.
    pub fn clocks_needed(&self, out_nsamp: Smp<u32>) -> Clk<u32> {
        self.blip_buf.clocks_needed(out_nsamp)
    }

    /// Makes input clocks before `clk_duration` available for reading as output
    /// samples. Also begins new time frame at `clk_duration`, so that clock time 0 in
    /// the new time frame specifies the same clock as `clk_duration` in the old time
    /// frame specified. Deltas can have been added slightly past `clk_duration` (up to
    /// however many clocks there are in two output samples).
    pub fn end_frame(&mut self, clk_duration: Clk<u32>) {
        self.blip_buf.end_frame(clk_duration)
    }

    /// Number of buffered samples available for reading.
    pub fn samples_avail(&self) -> Smp<u32> {
        self.blip_buf.samples_avail()
    }

    /// Reads and removes at most `buf.len()` samples and writes them to `buf`. If
    /// `stereo` is true, writes output to every other element of `buf`, allowing easy
    /// interleaving of two buffers into a stereo sample stream. Outputs 16-bit signed
    /// samples. Returns number of samples actually read.
    /// FIXME: if stereo is true: What if buf.len() is odd?
    /// If even, is return value "#elements=dtime*2" or "dtime=#elements/2"?
    pub fn read_samples(&mut self, buf: &mut [OutAmplitude<i16>], stereo: bool) -> Smp<usize> {
        self.blip_buf.read_samples(buf, stereo)
    }
}

#[cfg(test)]
mod test {
    use crate::blip_buf_units::BlipBufUnits;

    // Bring the macros and other important things into scope.
    use proptest::prelude::*;

    /// clocks_needed()
    #[test]
    fn clocks_needed_sufficient() {
        let mut blip = BlipBufUnits::new(4000);

        let clk_per_s = 1e6;
        let smp_s = 48000.0;
        blip.set_rates(clk_per_s, smp_s);

        let nsamp_needed = 2000;

        let clk_needed = blip.clocks_needed(nsamp_needed);
        blip.end_frame(clk_needed);
        assert!(blip.samples_avail() >= nsamp_needed);
        // assert!(blip.samples_avail() == nsamp_needed); (this test passes)
    }

    proptest! {
        /// Ensure that running the buffer for `clocks_needed()` produces enough output,
        /// given random parameters.
        #[test]
        fn clocks_needed_randomized(
            blip_nsamp in 100..4096,
            clk_per_s in 1_000_000..100_000_000,
            smp_s in 1000..400_000,
            nsamp_needed_ratio in 0.0..1.0,
        ) {
            let mut blip = BlipBufUnits::new(blip_nsamp as u32);

            blip.set_rates(clk_per_s as f64, smp_s as f64);

            let nsamp_needed = (blip_nsamp as f64 * nsamp_needed_ratio) as u32 + 1;

            let clk_needed = blip.clocks_needed(nsamp_needed);
            blip.end_frame(clk_needed);
            assert!(
                blip.samples_avail() >= nsamp_needed,
                format!("{} not >= {}", blip.samples_avail(), nsamp_needed)
            );
        }
    }

    /// Failing test case generated via proptest! clocks_needed_randomized.
    /// It appears that BlipBuf malfunctions when blip_nsamp is greater than 4096,
    /// and you ask for clocks_needed(4096) == 0.
    /// Filed at https://github.com/mvdnes/blip_buf-rs/issues/1
    #[test]
    #[should_panic]
    fn clocks_needed_test_case() {
        let blip_nsamp = 4374;
        let clk_per_s = 1e6;
        let smp_s = 1e3;

        // I added an assertion that blip_nsamp < 4096. This will panic.
        let mut blip = BlipBufUnits::new(blip_nsamp as u32);

        //        blip.set_rates(clk_per_s as f64, smp_s as f64);
        //
        //        let nsamp_needed = 4096;
        //
        //        let clk_needed = blip.clocks_needed(nsamp_needed);
        //
        //        // Why is nsamp_needed 4096 but clocks_needed 0?
        //        println!("{} smp = {} clocks", nsamp_needed, clk_needed);
        //        blip.end_frame(clk_needed);
        //        assert!(
        //            blip.samples_avail() >= nsamp_needed,
        //            format!("{} not >= {}", blip.samples_avail(), nsamp_needed)
        //        );
    }
}
