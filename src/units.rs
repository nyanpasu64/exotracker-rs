pub type Clk<T> = T;
pub type Cyc<T> = T;
pub type Smp<T> = T;
pub type Frame<T> = T;

pub type Cycles = u32;
pub type DeltaCycles = u32;

pub type Sec<T> = T;

pub type ClkPerS<T> = T;
pub type CycPerS<T> = T;
pub type SmpPerS<T> = T;
pub type FramePerS<T> = T;

pub type CycPerSmp<T> = T;
pub type SmpPerCyc<T> = T;
