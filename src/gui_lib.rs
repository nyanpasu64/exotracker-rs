use gtk::prelude::*;
use gtk::Orientation;

pub fn add_widget<P: gtk::IsA<gtk::Container>, W: gtk::IsA<gtk::Widget>>(parent: &P, w: W) -> W {
    parent.add(&w);
    w
}

// https://doc.qt.io/qt-5/qlayout.html#setContentsMargins says 11. Qt on Windows (Vista/Fusion) uses 9.
const BORDER: u32 = 9;
// Taken from Qt's default QHBoxLayout spacing. Qt on Windows (Vista/Fusion) uses 6.
const SPACING: i32 = 6;

pub fn gtk_frame(label: Option<&str>) -> gtk::Frame {
    let w = gtk::Frame::new(label);
    w.set_border_width(BORDER);
    w
}

pub fn gtk_box(orientation: Orientation) -> gtk::Box {
    let w = gtk::Box::new(orientation, SPACING);
    w.set_border_width(BORDER);
    w
}
