#![allow(non_upper_case_globals)]

mod blip_buf_units;
mod gui_lib;
mod units;

use gio::prelude::*;
use gtk::prelude::*;
use gtk::Application;
use lazy_static::lazy_static;

mod all_synths {
    use crate::blip_buf_units::BlipBufUnits;
    use crate::synth::{SoundChip, Synth};
    use crate::units::*;

    /// in 0CC, called APU.
    /// Owns all chips, owns audio synth, interacts with audio thread.
    pub struct AllApu {
        chips: Vec<Box<dyn SoundChip>>,

        // in rust, you can't use references into `chips`.
        // enabled_chips: Vec<&SoundChip>,
        synth: Synth,
    }

    impl AllApu {
        /// Combines 0CC's APU.add_time() and process().
        /// Unlike 0CC, this does not call end_frame.
        pub fn run_for_cycles(&mut self, dt: DeltaCycles) {
            for chip in self.chips.iter_mut() {
                chip.process(dt);
            }
        }

        /// Called by audio loop.
        pub fn end_frame(&mut self) {
            for chip in self.chips.iter_mut() {
                chip.write_to_synth(&mut self.synth);
            }
            // get audio from synth somehow
        }
    }
}

mod synth {
    //! does not interact with audio thread.

    use crate::blip_buf_units::BlipBufUnits;
    use crate::units::*;

    pub struct Synth {
        blip_buf: BlipBufUnits,
    }

    impl Synth {}

    /// Generally ranges from 0..16 for a PSG with 4-bit volume.
    /// Produced by a `SoundChannel`.
    /// Fed into a `SoundChip` synth, which mixes 1 or more channels into a Blip_Synth.
    pub type ChipAmplitude = i32;

    pub trait SoundChip {
        fn process(&mut self, dt: DeltaCycles); // , synth: &mut Synth

        fn write_to_synth(&mut self, synth: &mut Synth);
    }

    pub trait SoundChannel<OwnerChip: SoundChip> {
        /// we don't need &self AND amplitude. pick one or the other.
        fn set_amplitude(&self, amplitude: ChipAmplitude, chip: &mut OwnerChip);
    }
}

/// http://www.rossbencina.com/code/real-time-audio-programming-101-time-waits-for-nothing
mod audio {
    type Amplitude = i16;
    type AudioQueueBlock = Vec<Amplitude>;

    pub mod synth {
        use crate::audio::{Amplitude, AudioQueueBlock};
        use crossbeam::channel::Sender;
        use rand::Rng;
        use std::time::Duration;

        // Used to communicate with the synth thread.
        pub enum SynthMessage {
            PlayPause,
            Drum,
        }
        pub type SynthMessageQueue = crossbeam::queue::SegQueue<SynthMessage>;

        /// Used to terminate the synth thread.
        pub struct StopSynth();
        pub type StopSynthQueue = crossbeam::queue::ArrayQueue<StopSynth>;

        const WAIT_FOR_AUDIO_WRITE: Duration = Duration::from_secs(2);

        /// The synth thread receives input messages through `messages`, referencing a lock-free queue.
        /// Despite neither the main nor the synth thread having exclusive access to the queue,
        /// the main thread can write, and the synth thread can read, from the queue.
        ///
        /// Frames are "engine frames" or ticks.
        /// Blocks have constant size determined by output device/driver settings.
        pub struct AudioSynth<'a, 'b> {
            // TODO maybe make the queue finite, and drop excessive messages?
            // Is dropping messages good or bad?
            messages: &'a SynthMessageQueue,
            stop_synth: &'b StopSynthQueue,

            is_playing: bool,
            drum_volume: i16,

            stereo_nchan: usize,
            stereo_smp_per_block: usize,
            mono_smp_per_block: usize,

            frame_duration_nsamp: usize,
            block_buffer: Vec<Amplitude>,
            synth_to_output: Sender<AudioQueueBlock>,
        }

        impl AudioSynth<'_, '_> {
            /// `stereo_nchan` and `mono_smp_per_block` must both be positive and not zero.
            pub fn new<'a, 'b>(
                messages: &'a SynthMessageQueue,
                stop_synth: &'b StopSynthQueue,
                stereo_nchan: usize,
                mono_smp_per_block: usize,
                synth_to_output: Sender<AudioQueueBlock>,
            ) -> AudioSynth<'a, 'b> {
                let stereo_smp_per_block = mono_smp_per_block * stereo_nchan;

                let mut block_buffer = Vec::new();
                block_buffer.reserve_exact(stereo_smp_per_block);

                AudioSynth {
                    messages,
                    stop_synth,
                    is_playing: false,
                    drum_volume: 0,
                    stereo_nchan,
                    stereo_smp_per_block,
                    mono_smp_per_block,
                    frame_duration_nsamp: 800, // 48000/60 out of laziness
                    block_buffer,
                    synth_to_output,
                }
            }

            pub fn run(&mut self) {
                loop {
                    let stop_synth_maybe = self.run_frame();
                    if let Err(_stop_synth) = stop_synth_maybe {
                        return;
                    }
                }
            }

            fn run_frame(&mut self) -> Result<(), StopSynth> {
                // Check if the synth should terminate.
                if let Ok(stop_synth) = self.stop_synth.pop() {
                    eprintln!("Stopping synth before synthesizing");
                    return Err(stop_synth);
                }

                // Pop messages from the queue, until queue is empty: Err(PopError).
                while let Ok(m) = self.messages.pop() {
                    match m {
                        SynthMessage::PlayPause => {
                            self.is_playing = !self.is_playing;
                        }
                        SynthMessage::Drum => {
                            self.drum_volume = i16::max_value() / 2;
                        }
                    }
                }

                let mut frame_audio = Vec::new();
                frame_audio.resize(self.frame_duration_nsamp * self.stereo_nchan, 0);

                let mut rng = rand::thread_rng();
                if self.is_playing {
                    for y in frame_audio.iter_mut() {
                        *y = rng.gen::<i16>() / 3;
                    }
                } else {
                    for y in frame_audio.iter_mut() {
                        *y = 0;
                    }
                }

                for chan_y in frame_audio.chunks_exact_mut(self.stereo_nchan) {
                    if self.drum_volume > 0 {
                        let drum_val =
                            rng.gen_range::<i16, i16, i16>(-self.drum_volume, self.drum_volume);
                        for y in chan_y.iter_mut() {
                            *y += drum_val;
                        }
                        self.drum_volume -= 3;
                    } else {
                        break;
                    }
                }

                let mut frame_iter = frame_audio.chunks_exact(self.stereo_nchan);
                while let Some(chan_frame) = frame_iter.next() {
                    self.block_buffer.extend_from_slice(chan_frame);

                    assert!(self.block_buffer.len() <= self.stereo_smp_per_block);
                    if self.block_buffer.len() >= self.stereo_smp_per_block {
                        // May stop synth if output or channel is closed.
                        self.send_block_to_synth(self.block_buffer.clone())?;

                        // Does not reduce capacity, which is good since refilling the Vec is fast.
                        self.block_buffer.clear();
                    }
                }

                // Do not stop synth.
                Ok(())
            }

            fn send_block_to_synth(&mut self, mut block: AudioQueueBlock) -> Result<(), StopSynth> {
                use crossbeam::SendTimeoutError;

                loop {
                    match self
                        .synth_to_output
                        .send_timeout(block, WAIT_FOR_AUDIO_WRITE)
                    {
                        Err(SendTimeoutError::Timeout(x)) => {
                            eprintln!("Audio output is not responding, continuing");
                            // Check if the synth should terminate.
                            if let Ok(stop_synth) = self.stop_synth.pop() {
                                eprintln!("Stopping synth instead of sending audio to output");
                                return Err(stop_synth);
                            }

                            block = x;
                            continue;
                        }
                        Err(SendTimeoutError::Disconnected(_)) => {
                            eprintln!("Audio output is disconnected, stopping synth");
                            return Err(StopSynth());
                        }
                        Ok(()) => return Ok(()),
                    }
                }
            }
        }
    }

    pub mod output {
        use crate::audio::{Amplitude, AudioQueueBlock};
        use crossbeam::channel::Receiver;
        use sdl2::audio::AudioCallback;

        pub struct AudioOutput {
            output_from_synth: Receiver<AudioQueueBlock>,
        }

        impl AudioOutput {
            pub fn new(output_from_synth: Receiver<AudioQueueBlock>) -> AudioOutput {
                AudioOutput { output_from_synth }
            }
        }

        impl AudioCallback for AudioOutput {
            type Channel = Amplitude;

            fn callback(&mut self, out_buffer: &mut [Self::Channel]) {
                // TODO count underruns via Receiver.try_recv()
                let mut write_to_out = |from_synth: AudioQueueBlock| {
                    assert_eq!(out_buffer.len(), from_synth.len());
                    for (to, from) in out_buffer.iter_mut().zip(from_synth.iter()) {
                        *to = *from;
                    }
                };

                match self.output_from_synth.try_recv() {
                    Ok(from_synth) => return write_to_out(from_synth),
                    Err(_) => {
                        eprintln!("Synth not producing audio fast enough, audio queue underflow");
                    }
                }

                match self.output_from_synth.recv() {
                    Ok(from_synth) => return write_to_out(from_synth),
                    Err(_) => {
                        // The synth has been dropped. Write silence.
                        eprintln!("Synth is dead, continuing");
                        for y in out_buffer.iter_mut() {
                            *y = 0;
                        }
                    }
                }
            }
        }
    }

    use crate::audio::output::AudioOutput;
    use crate::audio::synth::{AudioSynth, StopSynthQueue, SynthMessageQueue};
    use sdl2::audio::{AudioDevice, AudioSpec, AudioSpecDesired};
    use std::thread;

    #[allow(dead_code)]
    pub struct AudioThreadHandle {
        pub synth_thread: thread::JoinHandle<()>,
        pub output_device: AudioDevice<AudioOutput>,
    }

    impl AudioThreadHandle {
        pub fn new(
            audio_subsystem: &sdl2::AudioSubsystem,
            messages: &'static SynthMessageQueue,
            stop_synth: &'static StopSynthQueue,
        ) -> AudioThreadHandle {
            use crossbeam::channel::bounded;

            // If `channels` > 1, at `samples` time apart,
            // SDL asks the callback to fill a 1D buffer of `[samples][channels]Amplitude`.
            let desired_spec = AudioSpecDesired {
                freq: Some(48000), // SDL defaults to 22050 on Linux, which is unacceptable.
                channels: Some(2), // stereo
                samples: Some(512),
            };

            let queue_nblocks = 1;

            let (sender, receiver) = bounded(queue_nblocks);
            let output_device = audio_subsystem
                .open_playback(None, &desired_spec, |_spec| {
                    let output = AudioOutput::new(receiver);
                    output
                })
                .unwrap();

            let spec: &AudioSpec = output_device.spec();
            let stereo_nchan = spec.channels as usize;
            let mono_smp_per_block = spec.samples as usize;
            println!("{}", mono_smp_per_block);

            let mut synth = AudioSynth::new(
                messages,
                stop_synth,
                stereo_nchan,
                mono_smp_per_block,
                sender,
            );
            let synth_thread = thread::Builder::new()
                .name("synth_thread".to_string())
                .spawn(move || {
                    synth.run();
                })
                .unwrap();

            // Start audio output
            output_device.resume();

            AudioThreadHandle {
                synth_thread,
                output_device,
            }
        }
    }
}

use audio::synth::{StopSynth, StopSynthQueue, SynthMessage, SynthMessageQueue};
use audio::AudioThreadHandle;

// gtk-rs expects all GUI callbacks to have 'static lifetime.
// Therefore, the audio message queue must be static.
lazy_static! {
    static ref messages_static: SynthMessageQueue = SynthMessageQueue::new();
}
lazy_static! {
    static ref stop_synth_static: StopSynthQueue = StopSynthQueue::new(1);
}

mod gui {
    use crate::gui_lib::*;
    use gtk::prelude::*;
    use gtk::Orientation;
    use gtk::{Application, ApplicationWindow, Button};

    pub struct MainWindowUi {
        pub play_pause: Button,
        pub drum: Button,
    }

    pub fn build_ui(app: &Application) -> MainWindowUi {
        let w = ApplicationWindow::new(app);
        w.set_title("exotracker");
        w.set_default_size(350, 70);

        let play_pause: Button;
        let drum: Button;

        {
            let w = add_widget(&w, gtk_frame(Some("uwu")));
            let w = add_widget(&w, gtk_box(Orientation::Vertical));
            {
                play_pause = add_widget(&w, Button::new_with_label("Play/Pause"));
                drum = add_widget(&w, Button::new_with_label("Drum"));
            }
        }
        w.show_all();
        MainWindowUi { play_pause, drum }
    }
}

use gui::build_ui;

fn main() {
    let application = Application::new(Some("org.nyanpasu64.exotracker"), Default::default())
        .expect("failed to initialize GTK application");

    // Sdl calls SDL_Init on creation and SDL_Quit on drop.
    let sdl_context = sdl2::init().unwrap();

    // AudioSubsystem calls SDL_InitSubSystem(SDL_INIT_AUDIO) on creation
    // and SDL_QuitSubSystem(SDL_INIT_AUDIO) on drop.
    let audio_subsystem = sdl_context.audio().unwrap();

    // So create both in main(), to avoid dropping them (which could cause bad things).

    // Begin playing audio. Dropping this variable makes audio stop.
    let audio = AudioThreadHandle::new(&audio_subsystem, &messages_static, &stop_synth_static);

    application.connect_activate(|app| {
        let ui = build_ui(app);
        ui.play_pause.connect_clicked(|_| {
            messages_static.push(SynthMessage::PlayPause);
        });
        ui.drum.connect_clicked(|_| {
            messages_static.push(SynthMessage::Drum);
        });
    });

    application.run(&[]);

    let AudioThreadHandle {
        synth_thread,
        output_device,
    } = audio;

    // The output is unpredictable and may never pull data if drivers are broken.
    // If the output never unblocks the synth, the synth will not respond to "terminate" requests.
    // Kill the output first, so the synth is immediately unblocked and terminates.
    std::mem::drop(output_device);
    // Dropping a SDL AudioDevice first calls SDL_CloseAudioDevice(),
    // which waits for the callback to not be running before deleting the device.
    // So dropping output_device is not undefined behavior (despite the
    // SDL wrapper API being unsound: https://github.com/Rust-SDL2/rust-sdl2/issues/870).

    stop_synth_static.push(StopSynth()).unwrap();
    synth_thread.join().unwrap();
}

/*
TODO add tests
that stopping the synth works
you can stop the synth, if the output reads, doesn't read, or is dropped.
ensure that if the output is dropped, the synth terminates immediately.
*/
